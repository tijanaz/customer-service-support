from flask import render_template, url_for, flash, redirect, request, abort
from flaskproj import app, db
from flaskproj.forms import LoginForm, PostForm, CommentForm
from flaskproj.models import User, Post
from flask_login import login_user, current_user, logout_user, login_required
from datetime import datetime


# home page
@app.route("/")
def home():
    return render_template('home.html')


# Here customers add a new post
@app.route("/newPost", methods=['GET', 'POST'])
def newPost():
    form = PostForm()
    if form.validate_on_submit():

        isUniqueNotOk = False
         
        if form.dateAndTimeForCallback.data == '':
           dateAndTime = datetime.now()
        else:
            dateAndTime = datetime.strptime(form.dateAndTimeForCallback.data, '%Y-%m-%d %H:%M')

        post = Post(comment='', 
                    name=form.name.data, 
                    phoneNumber = form.phoneNumber.data, 
                    company = form.company.data, 
                    email = form.email.data, 
                    subject=form.subject.data, 
                    problemDescription=form.problemDescription.data, 
                    dateAndTimeForCallback = dateAndTime, 
                    archive=False)
        uniqueNameCheck = Post.query.filter_by(name=form.name.data).first() 
        uniquePhoneNumberCheck = Post.query.filter_by(phoneNumber=form.phoneNumber.data).first()
        uniqueEmailCheck = Post.query.filter_by(email=form.email.data).first()

        if uniqueNameCheck :
            isUniqueNotOk = True
            flash('The name is taken. Please, write another one.', 'danger')
        elif uniquePhoneNumberCheck and form.phoneNumber.data != '-':
            isUniqueNotOk = True
            flash('The phone number already exists.', 'danger')
        elif uniqueEmailCheck:
            isUniqueNotOk = True
            flash('The email already exists.', 'danger')

        if isUniqueNotOk == False:
            db.session.add(post)
            db.session.commit()
            flash('Thank you for contacting us!', 'success')
            return redirect(url_for('home'))

    return render_template('post_create.html', title = 'New Post', form = form)


# login for admin 
@app.route("/login", methods=['GET', 'POST'])
def login():

    if  current_user.is_authenticated:
        return redirect(url_for('admin'))

    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data, password = form.password.data).first()
        
        if user :
            login_user(user)
            next_page = request.args.get('next')
            return redirect(next_page) if next_page else redirect(url_for('admin'))
        else:
            flash('Login Unsuccessful. Please check email and password', 'danger')

    return render_template('login.html', title='Login', form=form)


# logout for admin
@app.route("/logout")
def logout():
    logout_user()
    return redirect(url_for('home'))


#List of added posts from customers, admin can see this
@app.route("/admin")
def admin():
    
    page = request.args.get('page', 1, type=int)
    posts = Post.query.order_by(Post.dateAndTimeForCallback).filter(Post.archive==False).paginate(page=page, per_page = 2)
    
    return render_template('admin.html', posts = posts)


# sigle post page  
@app.route("/admin/<int:post_id>")
def post(post_id):

    post = Post.query.get_or_404(post_id)
    return render_template('posts.html', title = post.subject, post = post)


# post archiving
@app.route("/admin/<int:post_id>/archive", methods=['POST'])
def archive_post(post_id):

    post = Post.query.get_or_404(post_id)
    post.archive = True
    db.session.commit()
    flash('Post has been archived!', 'success')
    return redirect(url_for('admin'))
    
#add comment on post
@app.route("/admin/<int:post_id>/add_comment", methods=['GET', 'POST'])
def add_comment(post_id):

    post = Post.query.get_or_404(post_id)
    form = CommentForm()

    if form.validate_on_submit():
        post.comment = form.comment.data
        db.session.commit()
        flash('Your comment has been saved!', "success")
        return redirect(url_for('admin'))
        
    return render_template('add_comment.html', title = post.subject, post = post, form = form)
                           