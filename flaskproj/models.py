from datetime import datetime
from flaskproj import db, login_manager
from flask_login import UserMixin


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(user_id)


class User(db.Model, UserMixin):

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(120), unique=True, nullable=False)
    password = db.Column(db.String(60), nullable=False)

    def __repr__(self):
        return "User('{0}', '{1}', {2})".format(self.id, self.email, self.password)

class Post(db.Model):

    comment = db.Column(db.Text, nullable = False)
    archive = db.Column(db.Boolean, default=False)
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(20), unique = True, nullable = False)
    phoneNumber = db.Column(db.String(20), nullable = True)
    company = db.Column(db.String(200), nullable = True)
    email = db.Column(db.String(120), unique = True, nullable = False)
    subject = db.Column(db.String(200), nullable = False)
    problemDescription = db.Column(db.Text, nullable = False)
    dateAndTimeForCallback = db.Column(db.DateTime, nullable = True)
    
    def __repr__(self):
        return "Post('{0}', '{1}', {2})".format(self.name, self.subject, self.archive)