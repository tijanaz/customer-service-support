from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileAllowed
from flask_login import current_user
from wtforms import StringField, PasswordField, SubmitField, BooleanField, TextAreaField, DateField, TimeField, DateTimeField
from wtforms.validators import DataRequired, Length, Email, EqualTo, ValidationError
from flaskproj.models import User
from datetime import datetime

now = datetime.now()

class LoginForm(FlaskForm):
    email = StringField('Email',
                        validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    submit = SubmitField('Login')

def DayTimeValidation(data, field):   

    global now
    
    if field.data != '':
        try:
            datetime.strptime(field.data, '%Y-%m-%d %H:%M')
        except ValueError:
            raise ValueError("Incorrect data format, should be Year-Month-Day hours:minutes.")

        dateTime =  datetime.strptime(field.data, '%Y-%m-%d %H:%M')

        if dateTime and dateTime.date() != now.date():

            if dateTime.date() < now.date():
                raise ValidationError("Added time need to be in the future.")

            elif dateTime and dateTime.isoweekday() == 7:
                raise ValidationError("We don't work on Sundays.")

            elif dateTime and (dateTime.hour < 8 or dateTime.hour > 20) and dateTime.isoweekday() <= 5:
                raise ValidationError("We work from 8:00 to 20:00")

            elif dateTime and (dateTime.hour < 8 or (dateTime.hour == 13 and dateTime.minute > 0) \
                or dateTime.hour > 13) and dateTime.isoweekday() == 6:
                raise ValidationError("We work from 8:00 to 13:00 on Saturdays")

            elif dateTime and (dateTime.minute != 0 and dateTime.minute != 30):
                raise ValidationError("You can only send us a message in a full hour or half an hour.")

class PostForm(FlaskForm):

    global now

    name = StringField('Your name', validators=[DataRequired()])
    phoneNumber = StringField('Your phone', default='-')
    company = StringField('Your company')
    email = StringField('Your email', validators=[DataRequired(), Email()])
    subject = StringField('Subject', validators=[DataRequired()])
    problemDescription = TextAreaField('Problem description', validators=[DataRequired()])
    dateAndTimeForCallback = StringField('Enter date and time for callback', \
                                         default = '', validators=[DayTimeValidation])
    submit = SubmitField('Post')

class CommentForm(FlaskForm):

    comment = TextAreaField('Write comment here', validators=[DataRequired()])
    submit = SubmitField('Add comment')

