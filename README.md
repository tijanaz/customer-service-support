# Customer Service Support #

A callback form for customers to contact us.  
This project was generated with **Python Flask**.

Admin can log in and view customer posts, sorted by date and time, add comments and archive them.  
_(Admin email: 'admin@gmail.com' and password: 'password')_

## Installation and run

You need to have installed **Python3**.  
Check the version (mine is Python 3.5.2).
```sh
$ python3 --version
```
#### Then you need to install the following things:  ####
```sh
$ pip install flask 
```
```sh
$ pip install flask-login
```
```sh
$ pip install flask-wtf
```
```sh
$ pip install datetime
```
```sh
$ pip install flask-sqlalchemy
```
```sh
$ pip install flask-testing
```

#### Running application and tests:  ####
```sh
$ python3 run.py
```

If you get _OSError: [Errno 98] Address already in use_ error, enter this:
```sh
$ sudo lsof -i:5000
```
```sh
$ kill -9 $PID
```
Then run the application again.

Open your browser and enter this: _http://127.0.0.1:5000/_

Tests running:
```sh
$ python3 tests.py
```
