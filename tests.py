from flask_testing import TestCase
import unittest
from flaskproj import app, db
from flaskproj.models import User, Post
from flask_login import current_user
from datetime import datetime


class BaseTestCase(TestCase):

    # A base test case
    def create_app(self):
        app.config.from_object('config.TestConfig')
        return app

    # database tests
    def setUp(self):
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_user_setup(self):

        db.session.add(User(email = "ad@min.com", password = "admin"))
        db.session.commit()

    def test_post_setup(self):
        db.session.add(Post(name = "Test", \
                            phoneNumber = "065", \
                            company = "Test company", \
                            email = "test@gmal.com", \
                            subject = "Test subject", \
                            problemDescription = "This is a test. Only a test.", \
                            dateAndTimeForCallback = datetime.strptime("2022-08-13 10:00", '%Y-%m-%d %H:%M'), \
                            comment = ""))
        db.session.commit()
        

class FlaskTestCase(BaseTestCase):

    # Ensure that Flask was set up correctly
    # Check if Response is 200
    def test_index(self):
        tester = app.test_client(self)
        response = tester.get("/")
        statuscode = response.status_code
        self.assertEqual(statuscode, 200)

    def test_index_new_post(self):
        tester = app.test_client(self)
        response = tester.get("/newPost")
        statuscode = response.status_code
        self.assertEqual(statuscode, 200)
    
    def test_index_admin(self):
        tester = app.test_client(self)
        response = tester.get("/admin")
        statuscode = response.status_code
        self.assertEqual(statuscode, 200)

    def test_index_logint(self):
        tester = app.test_client(self)
        response = tester.get("/login")
        statuscode = response.status_code
        self.assertEqual(statuscode, 200)


    #check if content return is text/html; charset=utf-8
    def test_index_content(self):
        tester = app.test_client(self)
        response = tester.get('/')
        self.assertEqual(response.content_type, "text/html; charset=utf-8")

    def test_index_content_new_post(self):
        tester = app.test_client(self)
        response = tester.get('/newPost')
        self.assertEqual(response.content_type, "text/html; charset=utf-8")

    def test_index_content_admin(self):
        tester = app.test_client(self)
        response = tester.get('/admin')
        self.assertEqual(response.content_type, "text/html; charset=utf-8")

    def test_index_content_login(self):
        tester = app.test_client(self)
        response = tester.get('/login')
        self.assertEqual(response.content_type, "text/html; charset=utf-8")

        

if __name__ == '__main__':
    unittest.main()
 
